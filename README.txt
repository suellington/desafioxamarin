O aplicativo tem o objetivo de realizar consulta de endereços pelo CEP
A consulta online é feita usando o Webservice gratuito :https://viacep.com.br/
É realizada uma verificação de conexão do dispositivo
O endereço completo é exibido na tela inicial
Os resultados das pesquisas são salvos no banco local Sqlite
Outra página exibe uma lista de endereços salvos no banco local, onde clicando em cada um deles é mostrada outra página com seus detalhes
Não são salvos registros duplicados
Uma mensagem é exibida na tela (popup) avisando ao usuário que o CEP já foi gravado
Em um botão da tela principal é mostrado a quantidade de endereços armazenados no banco de dados local