﻿using DesafioXamarin.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace DesafioXamarin.Dados
{
    public class SqliteEndereco : IEndereco
    {
        private SQLiteAsyncConnection _conexao;

        public async Task InserirEndereco(Endereco endereco)
        {
            if (endereco.id != 0)
            {
                _ = await _conexao.UpdateAsync(endereco);
            }
            else
            {
                _ = await _conexao.InsertAsync(endereco);
            }
        }

        public Task<List<Endereco>> GetEnderecos()
            => _conexao.Table<Endereco>().ToListAsync();

        public async Task Initialize()
        {
            if (_conexao != null) return;

            _conexao = new SQLiteAsyncConnection(
                Path.Combine(Xamarin.Essentials.FileSystem.AppDataDirectory, "Enderecos.db"));

            await _conexao.CreateTableAsync<Endereco>();
        }

        public async Task<bool> GetEnderecoPeloCEP(string cep){
            var result = await _conexao.QueryAsync<Endereco>("SELECT * FROM Endereco Where cep=? LIMIT 1", cep);
            return result != null && result.Count > 0 ? true : false;
        }

        public bool DBExists()
        {
            string DocumentPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var path = Path.Combine(DocumentPath, "Enderecos.db");
            return File.Exists(path);
        }

    }
}
