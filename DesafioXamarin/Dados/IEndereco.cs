﻿using DesafioXamarin.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DesafioXamarin.Dados
{
    public interface IEndereco
    {
        Task Initialize();
        Task<List<Endereco>> GetEnderecos();
        Task InserirEndereco(Endereco endereco);
        Task<bool> GetEnderecoPeloCEP(string cep);
    }
}
