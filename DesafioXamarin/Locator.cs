﻿using DesafioXamarin.Dados;
using DesafioXamarin.Models;
using DesafioXamarin.Services;
using DesafioXamarin.ViewModels;
using DesafioXamarin.Views;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace DesafioXamarin
{
    public static class Locator
    {
        private static IServiceProvider serviceProvider;

        public static void Initialize()
        {
            var services = new ServiceCollection();
            services.AddSingleton<IEndereco, SqliteEndereco>();
            services.AddSingleton<IViaCEPService, ViaCEPService>();
            services.AddTransient<MainPageViewModel>();
            services.AddTransient<EnderecosViewModel>();
            services.AddTransient<EnderecosDetalhesViewModel>();
            services.AddTransient<MainPage>();
            services.AddTransient<EnderecosView>();
            services.AddTransient<EnderecoDetalhesView>();

            serviceProvider = services.BuildServiceProvider();
        }

        public static T Resolve<T>() => serviceProvider.GetService<T>();
    }
}
