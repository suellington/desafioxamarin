﻿using DesafioXamarin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DesafioXamarin.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EnderecosView : ContentPage
    {
        public EnderecosView(EnderecosViewModel viewModel)
        {
            InitializeComponent();
            viewModel.Navigation = Navigation;
            BindingContext = viewModel;
        }

        protected override void OnAppearing()
        {
            (BindingContext as EnderecosViewModel)?.CarregarEnderecos();
            base.OnAppearing();
        }

    }
}