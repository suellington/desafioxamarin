﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesafioXamarin.Models
{
    public class Endereco
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public string cep { get; set; }
        public string logradouro { get; set; }
        public string complemento { get; set; }
        public string bairro { get; set; }
        public string localidade { get; set; }
        public string uf { get; set; }
        public string unidade { get; set; }
        public string ibge { get; set; }
        public string gia { get; set; }
        public string ddd { get; set; }
        public string siafi { get; set; }
    }
}
