﻿using DesafioXamarin.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace DesafioXamarin.Services
{
    public class ViaCEPService : IViaCEPService
    {
        private static string URL = "https://viacep.com.br/ws/{0}/json/";

        public Endereco GetEnderecoViaCep(string cep)
        {
            var url = string.Format(URL, cep);
            WebClient wc = new WebClient();
            string result = wc.DownloadString(url);
            Endereco endereco = JsonConvert.DeserializeObject<Endereco>(result);
            if (endereco.cep == null)
                return null;
            return endereco;
        }
    }
}
