﻿using DesafioXamarin.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesafioXamarin.Services
{
    public interface IViaCEPService
    {
        Endereco GetEnderecoViaCep(string cep);
    }
}
