﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace DesafioXamarin.Utils
{
    public class TratarStrings
    {
        public string ApenasNumeros(string str)
        {
            var apenasNumeros = new Regex(@"[^\d]");
            return apenasNumeros.Replace(str, "");
        }

        public string FormataCep(string cep)
        {
            return Convert.ToUInt64(cep).ToString(@"00000\-000");
        }
    }
}
