﻿using DesafioXamarin.Dados;
using DesafioXamarin.Models;
using DesafioXamarin.Services;
using DesafioXamarin.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace DesafioXamarin.ViewModels
{
    public class EnderecosViewModel : BaseViewModel
    {
        private ObservableCollection<Endereco> _enderecos;
        public ObservableCollection<Endereco> Enderecos
        {
            get { return _enderecos; }
            set { SetProperty(ref _enderecos, value); }

        }

        private bool _pesquisando;
        public bool Pesquisando
        {
            get => _pesquisando;
            set => SetProperty(ref _pesquisando, value);
        }
        private readonly IEndereco endereco;
        public Command<Endereco> EnderecoSelecionadoCommand { get; set; }
        
        public EnderecosViewModel(IEndereco endereco)
        {
            this.endereco = endereco;

            this.EnderecoSelecionadoCommand = new Command<Endereco>(EnderecoSelecionado);
        }

        public async void CarregarEnderecos()
        {
            List<Endereco> enderecos = await endereco.GetEnderecos();
            Enderecos = new ObservableCollection<Endereco>(enderecos.OrderByDescending(e => e.cep));
        }

        private async void EnderecoSelecionado(Endereco endereco)
        {
            Pesquisando = true;
            
            var enderecoDetalhesView = Locator.Resolve<EnderecoDetalhesView>();
            var enderecoDetalhesViewModel = enderecoDetalhesView.BindingContext as EnderecosDetalhesViewModel;
            enderecoDetalhesViewModel.Endereco = endereco;

            await Navigation.PushAsync(enderecoDetalhesView);

            Pesquisando = false;
        }
    }
}
