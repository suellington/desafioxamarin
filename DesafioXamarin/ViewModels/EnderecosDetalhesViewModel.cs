﻿using DesafioXamarin.Models;
using DesafioXamarin.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace DesafioXamarin.ViewModels
{
    public class EnderecosDetalhesViewModel : BaseViewModel
    {
        private Endereco _endereco;
        public Endereco Endereco
        {
            get { return _endereco; }
            set { SetProperty(ref _endereco, value); }
        }

        public EnderecosDetalhesViewModel()
        {
            
        }
    }
}
