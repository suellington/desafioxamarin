﻿using DesafioXamarin.Dados;
using DesafioXamarin.Models;
using DesafioXamarin.Services;
using DesafioXamarin.Utils;
using DesafioXamarin.ViewModels;
using DesafioXamarin.Views;
using System.Collections.Generic;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace DesafioXamarin
{
    public class MainPageViewModel : BaseViewModel
    {
        private string _cep;
        public string CEP
        {
            get { return _cep; }
            set
            {
                SetProperty(ref _cep, value);
                if (value.Length < 9)
                    MostrarEndereco = false;
            }
        }

        private bool _pesquisando;
        public bool Pesquisando
        {
            get => _pesquisando;
            set => SetProperty(ref _pesquisando, value);
        }

        private Endereco _endereco;
        public Endereco Endereco
        {
            get { return _endereco; }
            set { SetProperty(ref _endereco, value); }

        }

        private int _quantidade = 0;
        public int Quantidade
        {
            get { return _quantidade; }
            set { SetProperty(ref _quantidade, value); }

        }

        private bool _mostrarEndereco = false;
        public bool MostrarEndereco
        {
            get { return _mostrarEndereco; }
            set { SetProperty(ref _mostrarEndereco, value); }
        }

        public Command PesquisarCommand { get; }
        public Command ModoOfflineCommand { get; }
        private TratarStrings tratarStrings;
        private readonly IViaCEPService viaCEPService;
        private readonly IEndereco endereco;

        public MainPageViewModel(IEndereco endereco, IViaCEPService viaCEPService)
        {
            this.endereco = endereco;
            this.viaCEPService = viaCEPService;

            tratarStrings = new TratarStrings();
            
            ModoOfflineCommand = new Command(ModoOffline);
            PesquisarCommand = new Command(Pesquisar);

        }

        private async void Pesquisar()
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                Pesquisando = true;
                if (!string.IsNullOrEmpty(CEP) && CEP.Length == 9)
                {
                    var cep = tratarStrings.ApenasNumeros(CEP);
                    var enderecoViaCEP = viaCEPService.GetEnderecoViaCep(cep);
                    
                    if (enderecoViaCEP != null)
                    {
                        Endereco = enderecoViaCEP;
                        MostrarEndereco = true;
                        var existe = await endereco.GetEnderecoPeloCEP(cep);
                        if (!existe)
                        {
                            enderecoViaCEP.cep = cep;
                            await endereco.InserirEndereco(enderecoViaCEP);
                            await Application.Current.MainPage.DisplayAlert("", "O CEP foi salvo no banco de dados local (Sqlite)", "OK");
                            Quantidade++;
                        }
                        else
                        {
                            await Application.Current.MainPage.DisplayAlert("", "O CEP já foi gravado", "OK");
                        }
                    }
                    else
                    {
                        MostrarEndereco = false;
                        await Application.Current.MainPage.DisplayAlert("", "CEP inválido", "OK");
                    }

                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("", "Digite um CEP válido", "OK");
                }
                Pesquisando = false;
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("", "Verifique sua conexão com a internet", "OK");
            }
        }

        public async void CarregarQuantidade()
        {
            List<Endereco> enderecos = await endereco.GetEnderecos();
            Quantidade = enderecos.Count;
        }

        private async void ModoOffline()
        {
            Pesquisando = true;

            var enderecosView = Locator.Resolve<EnderecosView>();
            await Navigation.PushAsync(enderecosView);

            Pesquisando = false;
        }
    }
}
