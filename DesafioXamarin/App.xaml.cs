﻿using DesafioXamarin.Dados;
using DesafioXamarin.Services;
using Xamarin.Forms;

namespace DesafioXamarin
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            Locator.Initialize();

            InitializeRepository();

            InitializeMainPage();
        }

        private void InitializeMainPage()
        {
            MainPage = new NavigationPage(Locator.Resolve<MainPage>());
        }

        private static void InitializeRepository()
        {
            IEndereco endereco = Locator.Resolve<IEndereco>();
            endereco.Initialize();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
